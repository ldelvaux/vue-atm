# vue-atm

The application was built with @ vue/cli, and uses Vue version 2.6.11.

Imagine an ATM machine with different currencies. A user can withdraw as much money as they want, provided the ATM has that currency.

The user chooses the currency they want to use, as well as the amount of money to withdraw. Then, the machine gives him his money back if it is available.
The ATM is programmed to give as few tickets as possible.

The ATM service contains the logic of ATM, as well as a dictionary containing the banknotes available by currency (RUB, EUR, UAH, USD, CUP, SOS).


Your role in this exercise is to use the ATM service to implement the interface of an ATM.
This is a relatively straightforward exercise aimed at seeing how you think, the best practices you implement, and the knowledge you have.

Have fun!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
