import currencies from "./currencies";

export const getCurrencies = () => {
  return Object.keys(currencies);
}

const getCurrencyNotes = (currency) => {
  if(!currency) {
    return [];
  }
  return currencies[currency];
}

export function giveBackMoney(currency, amount) {

  if(!currency) {
    return {"message": "You have to choose a currency.", "notes": []};
  }
  if(amount <= 0) {
    return {"message": "You have to specify the amount you want from the ATM.", "notes": []};
  }

  if(!getCurrencies().includes(currency)) {
    return {"message": "The ATM does'nt support the currency you choosed.", "notes": []};
  }

  if(amount % getCurrencyNotes(currency)[0]) {
    return {
      "message": `Can't do ${amount} ${currency}. Value must be divisible by ${getCurrencyNotes(currency)[0]}!`,
      "notes": []
    };
  }

  const notes = getCurrencyNotes(currency)
    .reduceRight((acc, v) => {
      const n = Math.floor(amount / v);
      amount %= v;

      return n ? `${acc}, ${n} * ${v} ${currency}` : acc;
    }, '')
    .substr(2);


    return {
      "message": `Operation completed.`,
      "notes": notes
    };
}
